💡목표

-자바스크립트의 addEventListener()함수를 이용하여, 버튼을 누를시에 스탑워치를 만든다. 
-setInterval()함수로 원하는 시간마다 코드를 실행시키고 , clearInterval()함수를 이용하여 setInterval()로 설정된 코드를 멈추는 방법을 배운다. 


📈 예상효과

- setInterval(),clearInterval()의 쓰임을 알게된다. 


💭 아키텍처 구성 및 접근방법 계획

1.startTimer버튼을 클릭하면 카운트다운을 시작한다. 이는 addEventListener()함수로 구현한다. <br/>
2.만들어진 함수안에, setInterval()를 통해서 1초마다 카운트다운을 해주는 함수가 실행되도록 한다. <br/>
(setInterval()함수는 어떤 코드를 일정한 시간 간격을 두고 반복해서 실행하고 싶을 때 사용한다. <br/>
setInterval(displayTimer,1000)는 1초동안 displayTimer라는 함수가 계속 실행된다는 뜻이다.) <br/>
3.카운트다운을 해주는 displayTimer()함수를 만들어준다.  <br/>
4.스탑워치는 초,분,시가 있기 때문에 일단 0으로 셋팅해준다.  <br/>
let [seconds,minutes,hours]=[0,0,0] <br/>
5.displayTimer()가 실행되면 , seconds를 1씩 증가시키고 , seconds가 60이 되는 순간, seconds를 0으로 바꾸고 minutes을 증가시킨다. minutes가 60이 되면  minutes를 0으로 만들고 hours를 증가시킨다.  <br/>
6.조건연산자를 넣어서 hours,minutes,seconds가 10보다 작으면, 01~09로 표시될 수 있도록 설정해둔다.  <br/>
7.만들어진 시간초 변수를 ineerHTML 을 이용하여 화면에 보여준다.  <br/>
8.resetTimer를 클릭한 경우, 초,분,시를 리셋하는 것이기 때문에 clearInterval()함수를 이용하여 setInterval()함수를 멈추고 , 시,분,초를 다시 0으로 설정한다.  <br/>
9.만들어진 변수를 innerHTML을 이용하여 화면에 출력한다. <br/>
10.pauseTimer를 클릭한 경우 , clearInterval()를 이용하여 setInterval()함수를 멈춘다. 이경우 리셋하는 과정이 아니라 변수들을 0으로 설정할 필요가 없다.  <br/>


🔗공부 내용정리

